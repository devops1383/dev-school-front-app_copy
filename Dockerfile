FROM timbru31/java-node:8-jdk-14 AS BUILD_IMAGE

ENV APP_HOME=/root/dev-school-front-app/
RUN mkdir -p $APP_HOME

WORKDIR $APP_HOME

COPY build.gradle gradlew gradlew.bat settings.gradle gradle.properties $APP_HOME
COPY gradle $APP_HOME/gradle
COPY devschool-front-app-client $APP_HOME/devschool-front-app-client
COPY devschool-front-app-server $APP_HOME/devschool-front-app-server

RUN apt update && apt install -y \
    python-dev \
    build-essential \
&& rm -rf /var/lib/apt/lists/*
RUN ./gradlew build

FROM openjdk:8-jre
WORKDIR /root/
COPY --from=BUILD_IMAGE /root/dev-school-front-app/devschool-front-app-server/build/libs/devschool-front-app-server-1.0.0.jar .
EXPOSE 8080
CMD ["java","-jar","devschool-front-app-server-1.0.0.jar"]
